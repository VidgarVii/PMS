require "application_system_test_case"

class FermasTest < ApplicationSystemTestCase
  setup do
    @ferma = fermas(:one)
  end

  test "visiting the index" do
    visit fermas_url
    assert_selector "h1", text: "Fermas"
  end

  test "creating a Ferma" do
    visit fermas_url
    click_on "New Ferma"

    fill_in "Description", with: @ferma.description
    fill_in "Hoster", with: @ferma.hoster
    fill_in "Name", with: @ferma.name
    fill_in "Url", with: @ferma.url
    click_on "Create Ferma"

    assert_text "Ferma was successfully created"
    click_on "Back"
  end

  test "updating a Ferma" do
    visit fermas_url
    click_on "Edit", match: :first

    fill_in "Description", with: @ferma.description
    fill_in "Hoster", with: @ferma.hoster
    fill_in "Name", with: @ferma.name
    fill_in "Url", with: @ferma.url
    click_on "Update Ferma"

    assert_text "Ferma was successfully updated"
    click_on "Back"
  end

  test "destroying a Ferma" do
    visit fermas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ferma was successfully destroyed"
  end
end
