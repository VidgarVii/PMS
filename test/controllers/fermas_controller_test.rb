require 'test_helper'

class FermasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ferma = fermas(:one)
  end

  test "should get index" do
    get fermas_url
    assert_response :success
  end

  test "should get new" do
    get new_ferma_url
    assert_response :success
  end

  test "should create ferma" do
    assert_difference('Ferma.count') do
      post fermas_url, params: { ferma: { description: @ferma.description, hoster: @ferma.hoster, name: @ferma.name, url: @ferma.url } }
    end

    assert_redirected_to ferma_url(Ferma.last)
  end

  test "should show ferma" do
    get ferma_url(@ferma)
    assert_response :success
  end

  test "should get edit" do
    get edit_ferma_url(@ferma)
    assert_response :success
  end

  test "should update ferma" do
    patch ferma_url(@ferma), params: { ferma: { description: @ferma.description, hoster: @ferma.hoster, name: @ferma.name, url: @ferma.url } }
    assert_redirected_to ferma_url(@ferma)
  end

  test "should destroy ferma" do
    assert_difference('Ferma.count', -1) do
      delete ferma_url(@ferma)
    end

    assert_redirected_to fermas_url
  end
end
