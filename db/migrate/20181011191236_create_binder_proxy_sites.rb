class CreateBinderProxySites < ActiveRecord::Migration[5.2]
  def change
    create_table :fermas_proxies, :id => false do |t|
      t.integer :ferma_id
      t.integer :proxy_id
    end
  end
end
