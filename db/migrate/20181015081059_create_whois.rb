class CreateWhois < ActiveRecord::Migration[5.2]
  def change
    create_table :whois do |t|
      t.references :ferma, foreign_key: true
      t.text :expires

      t.timestamps
    end
  end
end
