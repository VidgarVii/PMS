class CreateProxies < ActiveRecord::Migration[5.2]
  def change
    create_table :proxies do |t|
      t.string :name
      t.string :url
      t.integer :port
      t.string :login
      t.string :password

      t.timestamps
    end
  end
end
