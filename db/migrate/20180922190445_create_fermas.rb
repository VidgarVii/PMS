class CreateFermas < ActiveRecord::Migration[5.2]
  def change
    create_table :fermas do |t|
      t.string :name
      t.string :url
      t.string :hoster
      t.text :description
      t.belongs_to :user

      t.timestamps
    end
  end
end
