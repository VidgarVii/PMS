Rails.application.routes.draw do
  resources :fermas
  resources :proxies
  resources :projects do
    resources :tasks
  end
  devise_for :users

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
  get '/parser/', to: 'parser#index'
  get '/adminka/', to: 'adminka#index'
  post '/projects/:project_id/tasks/:id/status', to: 'tasks#status'
  post '/fermas/checkssl/', to: 'fermas#checkssl'
  post '/fermas/status200/', to: 'fermas#status200'
  post '/fermas/delete_proxy/', to: 'fermas#delete_proxy'
  post '/adminka/setstatus/', to: 'adminka#setstatus'
  post '/proxies/create/', to: 'proxies#create'

  root 'index#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
