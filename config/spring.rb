%w[
  .ruby-version
  .rbenv-vars
  tmp/restart.txt
  tmp/caching-dev.txt
  app/assets/*
  app/views/*
  app/views/tasks/show.html.slim
].each { |path| Spring.watch(path) }
