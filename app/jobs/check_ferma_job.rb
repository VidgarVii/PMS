class CheckFermaJob < ApplicationJob
  queue_as :default
  around_perform :around_cleanup

  def perform(*args)
      check_status    
  end

  require "http"
  def check_status
    redis = Redis.new
    sites = Ferma.all
    sites.each do |site|
      ip_proxy = site.proxies[0].url
      port_roxy = site.proxies[0].port
      login_proxy = site.proxies[0].login
      pass_proxy = site.proxies[0].password

      begin
        status = HTTP.via(ip_proxy, port_roxy, login_proxy, pass_proxy).head(site.url).status
        puts status
        redis.set site.url, status
      rescue Exception => e
        puts e
      end
      sleep 10
    end
    sleep 60*60   
  end  

  private

  def around_cleanup
    puts 'Запуск проверки'
    yield
    perform
  end

end
