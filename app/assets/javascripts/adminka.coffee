# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
    $('.edit').click (e) ->
        e.preventDefault()
        $('.modal-edit_status').css('left',(e.pageX+20)+'px').css('top',(e.pageY-15)+'px'); 
        $('.modal-edit_status').fadeIn()
        $('#modal_status_user').val($(e.target).attr('data-status'))
        $('#id').html($(e.target)[0].id)
        $(document).mouseup (e) -> 
            container = $('.modal-edit_status')
            if container.has(e.target).length == 0
               container.hide()        
        
    $('#modal_sbmt').click () ->
        status = $('#modal_status_user').val()
        id = $('#id').text()
        data = {'status' : status, 'id': id}
        $.ajax({
            type: 'POST',
            url: "/adminka/setstatus/",
            data: data,
            beforeSend: (xhr) ->
                xhr.setRequestHeader("X-CSRF-Token", $("meta[name='csrf-token']").attr("content"))
            success: (repsonse) ->
                $('.status')[id-1].innerHTML = repsonse
                $('.modal-edit_status').fadeOut()
            }
        )
    $('a#1').fadeOut()