# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$('html').on('click', '#check', () -> 
    checkssl()
    status200()
    repeat()
)

status200 = () ->
    statuses = $('.status')
    urls = []
    $('.url').each( (index) ->
        statuses[index].innerHTML = "- - -"
        data = {'url' : $( this ).text()}
        $.ajax({
            type: 'POST',
            url: "/fermas/status200",
            data: data,
            beforeSend: (xhr) ->
                xhr.setRequestHeader("X-CSRF-Token", $("meta[name='csrf-token']").attr("content"))
            success: (repsonse) ->				                
                statuses[index].innerHTML = repsonse.status
                statuses[index].classList.add(repsonse.class)
            }
        )
    )
checkssl = () ->
    ssls = $('.ssl')
    urls = []
    $('.url').each( (index) ->
        ssls[index].innerHTML = "- - -"
        data = {'url' : $( this ).text()}
        $.ajax({
            type: 'POST',
            url: "/fermas/checkssl",
            data: data,
            beforeSend: (xhr) ->
                xhr.setRequestHeader("X-CSRF-Token", $("meta[name='csrf-token']").attr("content"))
            success: (repsonse) ->
                if repsonse.length < 40
                    ssls[index].classList.add('bg-danger') 
                
                ssls[index].innerHTML = repsonse
            }
        )
    )
repeat = () ->
    setTimeout( () ->
        checkssl()
        status200()
        repeat()
    ,1000*60*60)
$('html').on('click', '#ferma_date_create_hosting', () ->
    $('#ferma_date_create_hosting').datepicker()
)

$(document).on "turbolinks:load", ->
    del_proxy = document.getElementsByClassName('js-btn-destroy-proxy')
    $(del_proxy).on 'click', () ->		
        $.ajax({
            type: 'POST',
            url: "/fermas/delete_proxy/",
            data: {'proxy_id':this.dataset.proxy_id, 'ferma_id': this.dataset.ferma_id },
            beforeSend: (xhr) ->
                xhr.setRequestHeader("X-CSRF-Token", $("meta[name='csrf-token']").attr("content"))
            success: (repsonse) ->

            }
        ) 
        
    