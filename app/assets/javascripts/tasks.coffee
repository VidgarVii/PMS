# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
    status = (status) ->
        switch (status)
            when '1' then return '<span class="status-importanty">Важный</span>'
            when '2' then return '<span class="status-medium">Средний</span>'
            when '3' then return '<span class="status-defer">Можно отложить</span>'
            when '4' then return '<span class="status-ready">Готово</span>'
            when '5' then return '<span class="status-test"> Протестировано</span>'

    $('#status').change( () -> 
        project_id = location.pathname.split('/')[2]
        id = location.pathname.split('/')[4]
        data = {'status' : $('#status').val()}
        $.ajax({
            type: 'POST',
            url: "/projects/#{project_id}/tasks/#{id}/status/",
            data: data,
            complete: () ->
                $('.task_header span').html(status($('#status').val()))
            }
        )
    )
