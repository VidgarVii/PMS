class Project < ApplicationRecord

    has_many :tasks, dependent: :destroy

    dragonfly_accessor :image

    validates_presence_of :name
 
    #image validations
    validates_presence_of :image
    validates_size_of :image, maximum: 600.kilobytes,
                      message: "should not be more than 600KB", if: :image_changed?
   
    validates_property :format, of: :image, in: ['jpeg', 'png', 'gif'],
                        message: "the formats allowed are: .jpeg, .png, .gif", if: :image_changed?
end
