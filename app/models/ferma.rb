class Ferma < ApplicationRecord    
	has_one :whoi, dependent: :destroy
	belongs_to :user
	has_and_belongs_to_many :proxies
	
	validates :date_create_hosting, presence: true, length: { in: 1..15 }
	validates :url, presence: true, length: { in: 1..50 }
	validates_associated :proxies
	validates :url, format: { with: /\Ahttps:\/\/(www)?\..*\z/, message: 'URL invalid!' }
end
