class Task < ApplicationRecord
  belongs_to :project
  validates :name, presence: true
  validates :status, presence: true, length: { in: 1..5 }
end
