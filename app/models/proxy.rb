class Proxy < ApplicationRecord
    has_and_belongs_to_many :fermas
    
    validates_presence_of :url
    validates_presence_of :port
    validates :url, format: { with: /\A(\d{1,3}\.){3}\d{1,3}\z/, message: 'IP invalid!' }
end
