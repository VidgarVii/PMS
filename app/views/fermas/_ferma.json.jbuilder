json.extract! ferma, :id, :name, :url, :hoster, :description, :created_at, :updated_at
json.url ferma_url(ferma, format: :json)
