require "whois-parser"
require 'ssl_scanner'
require "addressable/uri"

module FermasHelper
   
    def check_ssl url        
            ssl = SslScanner.scan_domain(url) 
            data = "Expires: #{ssl[:expires]} <br>
                    Check: #{ssl[:issuer]}"   
            return data  
    end

    def dns url
        uri = Addressable::URI.parse url
        #data = Whois.whois uri.host.gsub(/www./, '')
        return uri #data.parser.expires_on
    end
    

end
