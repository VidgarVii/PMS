module TasksHelper
    def status_to number
        case number
            when 1
                return '<span class="status-importanty">Важный</span>'
            when 2
                return '<span class="status-medium">Средний</span>'
            when 3
                return '<span class="status-defer">Можно отложить</span>'
            when 4
                return '<span class="status-ready">Готово</span>'
            when 5
                return '<span class="status-test"> Протестировано</span>'
        end
    end

end
