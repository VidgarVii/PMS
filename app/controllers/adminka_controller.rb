class AdminkaController < ApplicationController
    before_action :authenticate_user!

    def index
        status = current_user.status
        if status != 'admin'
          redirect_to root_path
        end
        @users = User.all
    end

    def setstatus
        status = params[:status]
        id = params[:id]
        user = User.find(id)
        user.update(status: status)
        render :html => user.status
    end
end
