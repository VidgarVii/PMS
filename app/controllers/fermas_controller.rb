class FermasController < ApplicationController
  before_action :authenticate_user!
  before_action :set_ferma, only: [:show, :edit, :update, :destroy]


  # GET /fermas
  # GET /fermas.json
  def index
    @statuses = {}
    if current_user.status == 'admin'|| current_user.status == 'happy'
      @fermas = Ferma.all
    elsif current_user.status == 'manager'
      id = current_user.id
      @fermas = Ferma.where(user_id:id)
    else
      redirect_to root_path
    end
  end

  # GET /fermas/1
  # GET /fermas/1.json
  def show
  end

  # GET /fermas/new
  def new
    @ferma = Ferma.new
    @proxies = Proxy.all  
  end

  # GET /fermas/1/edit
  def edit
    @proxies = Proxy.all    
  end

  # POST /fermas
  # POST /fermas.json
  def create
    @ferma = Ferma.new(ferma_params)
    @ferma.user_id = current_user.id
    proxy = Proxy.where(id: params[:proxy])
    @ferma.proxies << proxy
    whois = Whoi.create(ferma_id: @ferma.id, expires: whois(@ferma.url))
    @ferma.whoi = whois
    respond_to do |format|
      if @ferma.save
        format.html { redirect_to @ferma, notice: 'Ferma was successfully created.' }
        format.json { render :show, status: :created, location: @ferma }
      else
        format.html { render :new }
        format.json { render json: @ferma.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fermas/1
  # PATCH/PUT /fermas/1.json
  def update
    whois = Whoi.create(ferma_id: @ferma.id, expires: whois(@ferma.url))
    @ferma.whoi = whois
    respond_to do |format|
      if @ferma.update(ferma_params)
        proxy = Proxy.where(id: params[:proxy])
        @ferma.proxies << proxy if !@ferma.proxies.include? proxy[0]      
        
        format.html { redirect_to @ferma, notice: 'Ferma was successfully updated.' }
        format.json { render :show, status: :ok, location: @ferma }
      else
        format.html { render :edit }
        format.json { render json: @ferma.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fermas/1
  # DELETE /fermas/1.json
  def destroy
    if current_user.status == 'admin' || current_user.status == 'happy'
      @ferma.destroy
      respond_to do |format|
        format.html { redirect_to fermas_url, notice: 'Ferma was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  def delete_proxy
    #puts params[:proxy_id]
    @site = Ferma.find(params[:ferma_id])
    @site.proxies.delete(Proxy.find(params[:proxy_id]))
    #render html: "OK"
    redirect_to ferma_path(params[:ferma_id])
  end
  
  require "http"
  def status200
    url = params[:url].to_s
    site = Ferma.where(url: url)[0]
    @response = if site.proxies[0]
                  ip_proxy = site.proxies[0].url
                  port_roxy = site.proxies[0].port
                  login_proxy = site.proxies[0].login
                  pass_proxy = site.proxies[0].password
                  begin
                    status = HTTP.via(ip_proxy, port_roxy, login_proxy, pass_proxy).head(url).status
                  rescue Exception => e
                    puts e
                    if site.proxies[1]
                      begin
                        status = HTTP.via(site.proxies[1].url,  site.proxies[1].port, site.proxies[1].login,   site.proxies[1].password).head(url).status
                      rescue Exception => e
                        status = e
                      end
                    else
                      status = e
                    end
                  end
                  if status==200
                    add_class = "success"
                  else 
                    add_class = "bg-warning"                   
                  end
                  @response = {"status": status, "class": add_class}
                  #Status
                  #407 Proxy Authentication Required
                else
                  '{"status":"No Proxy", "class": "bg-warning"}'
                end
    render :json => @response
  end  

  require 'ssl_scanner'
  def checkssl    
    url = params[:url].to_s
    ssl = SslScanner.scan_domain(url) 
    @data = "Expires: #{ssl[:expires]} <br>
             Check: #{ssl[:issuer]}"
    render :html => @data.html_safe
  end

  private

    require "whois-parser"
    require "addressable/uri"
    def whois url    
      uri = Addressable::URI.parse url
      data = Whois.whois(uri.host.gsub(/www./, ''))
      return data.parser.expires_on    
    end  

    # Use callbacks to share common setup or constraints between actions.
    def set_ferma
      @ferma = Ferma.find(params[:id])
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def ferma_params
      params.require(:ferma).permit(:date_create_hosting, :name, :url, :hoster, :description, :user_id)
    end
end
