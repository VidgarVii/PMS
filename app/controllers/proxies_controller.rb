class ProxiesController < ApplicationController
    before_action :authenticate_user!
    before_action :set_proxy, only: [:show, :edit, :update, :destroy]

    def index
        if current_user.status == 'admin'|| current_user.status == 'happy'
            @proxies = Proxy.all
        else
            redirect_to root_path
        end
    end
    
    def show
    end

    def new
        @proxy = Proxy.new
    end
    
    def create 
       @proxy = Proxy.create(name: params[:name],url: params[:url], port: params[:port], login: params[:login], password: params[:password])
       if @proxy.save
        redirect_to proxies_path
       end
    end

    def destroy
        if current_user.status == 'admin'
            @proxy.destroy
            respond_to do |format|
            format.html { redirect_to proxies_path, notice: 'Project was successfully destroyed.' }
            format.json { head :no_content }
            end
        else
            render layout: false
        end
    end
    
    private

    def set_proxy
        @proxy = Proxy.find(params[:id])
    end

    def proxy_params
        params.require(:proxy).permit(:name, :url, :port, :login, :password)
    end

end
