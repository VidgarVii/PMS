class TasksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_task, only: [:status, :show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token, :only => [:status]

  def create
    @project = Project.find(params[:project_id])
    @task = @project.tasks.create(task_params)
    redirect_to project_path(@project)

  end

  def show  
  end

  def edit    
  end
  
  def update
    @project = Project.find(params[:project_id])
    @task = @project.tasks.find(params[:id])
    if @task.update(task_params)
      redirect_to project_path(@project)
  end
  end
  
  def status
    @project = Project.find(params[:project_id])
    @task = @project.tasks.find(params[:id])
    st = params[:status]
    @task.update(status: st)
    
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @project = Project.find(params[:project_id])
      @task = @project.tasks.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:name, :title, :status, :project_id)
    end
end
